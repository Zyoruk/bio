/* example using https://github.com/dougmoscrop/serverless-http */
import serverless from "serverless-http";
import cors from "cors";
import compression from "compression";
import moment from "moment";
import express from "express";
import axios from "axios";

const app = express();
const router = express.Router();
const GITLAB_UID = process.env.GITLAB_UID || 0;
const GITLAB_TOKEN = process.env.GITLAB_TOKEN || "";
const GITHUB_USERNAME = process.env.GITHUB_USERNAME || "";
const GITHUB_TOKEN = process.env.GITHUB_TOKEN || "";
const GITLAB_BASE_PATH = "https://gitlab.com/api/v4";
const GITHUB_BASE_PATH = "https://api.github.com";
const GITLAB_HOST = "gitlab";
const GITHUB_HOST = "github";
const AXIOS_TIMEOUT = 2000;

// gzip responses
router.use(compression());

router.get("/projects", function (req, res) {
  try {
    if (req.query.platform) {
      if (req.query.platform === GITLAB_HOST) {
        const axiosInstance = axios.create({
          baseURL: GITLAB_BASE_PATH,
          timeout: AXIOS_TIMEOUT,
          headers: { "Private-Token": GITLAB_TOKEN },
        });

        axiosInstance
          .get(`/users/${GITLAB_UID}/projects`, {
            params: { simple: true, visibility: "public" },
          })
          .then((response) => {
            const tempResponse = response;
            tempResponse.data = tempResponse.data.map((proj) => {
              // This will map the Gitlab data to something smaller for our client
              return {
                name: proj["name"],
                last_activity_at: moment(proj["last_activity_at"]).format(
                  "MMM Do, YYYY"
                ),
                web_url: proj["web_url"],
                star_count: proj["star_count"],
                forks_count: proj["forks_count"],
                description: proj["description"],
              };
            });
            res.send(tempResponse.data);
          })
          .catch((error) => res.status(500).send(error));
      } else if (req.query.platform === GITHUB_HOST) {
        const axiosInstance = axios.create({
          baseURL: GITHUB_BASE_PATH,
          timeout: AXIOS_TIMEOUT,
          headers: {
            Authorization: `token ${GITHUB_TOKEN}`,
          },
        });

        axiosInstance
          .get(`/users/${GITHUB_USERNAME}/repos`)
          .then((response) => {
            const tempResponse = response;
            tempResponse.data = tempResponse.data
              .filter((proj) => proj["fork"] === false)
              .map((proj) => {
                return {
                  name: proj["name"],
                  last_activity_at: moment(proj["updated_at"]).format(
                    "MMM Do, YYYY"
                  ),
                  web_url: proj["html_url"],
                  description: proj["description"],
                };
              });
            res.send(tempResponse.data);
          })
          .catch((error) => res.status(500).send(error)); // Likely that the API rate was exceeded: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting
      }
    } else {
      res.status(404).send("No plafform specified");
    }
  } catch (error) {
    res.status(500).send(error);
  }
});

app.use("/.netlify/functions/server/", router); // path must route to lambda
// Apply express middlewares
router.use(cors());

// Export lambda handler
exports.handler = serverless(app);
