const path = require("path");

module.exports = {
  webpack: {
    alias: {
      react: path.resolve(__dirname, "./node_modules/react"),
      hooks: path.join(__dirname, "src", "hooks"),
      interfaces: path.join(__dirname, "src", "interfaces"),
      services: path.join(__dirname, "src", "services"),
    },
  },
};
