export type CustomServerResponse<T> = {
  ok: boolean;
  status: number;
  body: T;
}
