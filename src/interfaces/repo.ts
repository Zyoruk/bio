export type Repo = {
  name: string;
  last_activity_at: string;
  web_url: string;
  description: string;
}

export type RepoVM = {
  name: string;
  lastActivity: string;
  url: string;
  description: string;
}
