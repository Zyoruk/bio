export type CustomResponse<T> = {
  status: number;
  data: T;
}
