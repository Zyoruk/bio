export const HTTP_CODE_2XX = 200;
export const HTTP_CODE_5XX = 500;
export const HTTP_CODE_4XX = 400;
export const BAD_RESPONSE = "bad response";
export const BAD_REQUEST = "bad request";
export const BAD_SERVER = "server error";
export const ERROR_MESSAGE = "Error";
export const BASE_PATH = "/.netlify/functions/server";