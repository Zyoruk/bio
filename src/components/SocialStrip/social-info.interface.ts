export type SocialNetwork = "gitlab" | "github" | "linkedin";

export interface SocialInfo {
  socialNetwork: SocialNetwork;
  username: string;
}
