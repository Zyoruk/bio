import { Grid, makeStyles } from "@material-ui/core";
import { CardInfo, CardItem } from "./CardItem";

interface CardListProps {
  cardsInfo: CardInfo[];
}

const useStyles = makeStyles((_) => {
  return {
    cardContainer: {
      padding: "1.5rem",
    },
    cardList: {
      paddingLeft: "1.5rem",
      paddingRight: "1.5rem",
    },
  };
});
export const CardList: React.FC<CardListProps> = ({ cardsInfo }) => {
  const styles = useStyles();
  return (
    <Grid className={styles.cardList} container>
      {cardsInfo.map((card) => {
        return (
          <Grid
            item
            className={styles.cardContainer}
            xs={12}
            md={6}
            lg={4}
            xl={4}
            key={card.url.toLowerCase()}
          >
            <CardItem {...card} />
          </Grid>
        );
      })}
    </Grid>
  );
};
